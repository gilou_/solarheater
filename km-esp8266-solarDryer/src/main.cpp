/*********
        Rui Santos
    Complete project details at https://randomnerdtutorials.com/esp8266-dht11dht22-temperature-and-humidity-web-server-with-arduino-ide/
*********/

// Import required libraries
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <LittleFS.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>

// Replace with your network credentials
const char *ssid = "SolarDryer";
const char *password = "supergigi";

FS *filesystem = &LittleFS;

#define DHTPIN 5 // Digital pin connected to the DHT sensor

// Uncomment the type of sensor in use:
//#define DHTTYPE        DHT11         // DHT 11
#define DHTTYPE DHT22 // DHT 22 (AM2302)
//#define DHTTYPE        DHT21         // DHT 21 (AM2301)

uint8_t statusLed = D6;	  // Here is a LED, bright give info (TBD)
uint8_t dryingFan = D7;	  // Here is the first 2 fans for extracting humid air
uint8_t regulState = 0;	  // at startup, the regulation is off
uint8_t powerMosfet = D5; // supply mosfet, in order to power the fans

DHT dht(DHTPIN, DHTTYPE);

// Global variables
float temperature = 0.0; // internal temperature
float humidity = 0.0;	 // internal humidity
int dryingDC = 10;		 // drying fan duty cycle, need to be between 10 to 100% (10 to 255)
float targetValue = 37;	 // Temperature set point

float sumIntegral;

// PID coefficients
int	COEFF_P	= 20;
int	COEFF_I	= 5;
int	COEFF_D	= 0.5;

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0; // will store last time DHT was updated

// Updates DHT readings every 1 seconds
const long interval = 1000;

const char *PARAM_INPUT_1 = "tempTarget";



int pid(float current, float target, float p, float i, float d){
// Read temperature as Celsius (the default)
		// calcul delta between current value and target value 
		// if current value inf to target value delta is negative
		float delta = current - target;
		sumIntegral += delta;

		return (int)(delta*p+sumIntegral*COEFF_I);
}

// Replaces placeholder with DHT values
String processor(const String &var)
{
	//Serial.println(var);
	if (var == "TEMPERATURE")
	{
		return String(temperature);
	}
	else if (var == "HUMIDITY")
	{
		return String(humidity);
	}
	else if (var == "DRYING")
	{
		return String(dryingDC / 10);
	}
	return String();
}

void setup()
{
	// Serial port for debugging purposes
	Serial.begin(115200);

	// Initialize LittleFS
	if (!LittleFS.begin())
	{
		Serial.println("An Error has occurred while mounting LittleFS");
		return;
	}

	dht.begin();

	// initialize mosfet pins
	pinMode(powerMosfet, OUTPUT);
	digitalWrite(powerMosfet, LOW);

	// Initialize timer for LEDs and FANs controls
	analogWriteFreq(19);			  // Initialize 19 kHz frequency
	analogWrite(statusLed, dryingDC); // Set duty cycle to 20% at wake-up
	analogWrite(dryingFan, dryingDC); // Set duty cycle to 20% at wake-up

	// Connect to Wi-Fi
	Serial.println(" ");
	Serial.println("Solar dryer v 0.1.1");
	Serial.println("Setting AP SolarDryer... ");
	WiFi.softAP(ssid, password);

	IPAddress IP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(IP);
	Serial.println(WiFi.softAPSSID());

	// Route for root / web page
	server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
		request->send(LittleFS, "/index.html", String(), false);
	});

	// Route to load fontawesome.css file
	server.on("/fontawesome.css", HTTP_GET, [](AsyncWebServerRequest *request) {
		request->send(LittleFS, "/fontawesome.css", "text/css");
	});
	// Route to load style.css file
	server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request) {
		request->send(LittleFS, "/style.css", "text/css");
	});
	// Route to load style.css file
	server.on("/webfonts/fa-solid-900.woff", HTTP_GET, [](AsyncWebServerRequest *request) {
		request->send(LittleFS, "/webfonts/fa-solid-900.woff", "font/woff");
	});
	server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request) {
		request->send_P(200, "text/plain", String(temperature).c_str());
	});
	server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request) {
		request->send_P(200, "text/plain", String(humidity).c_str());
	});
	server.on("/drying", HTTP_GET, [](AsyncWebServerRequest *request) {
		request->send_P(200, "text/plain", String(dryingDC / 10).c_str());
	});
	server.on("/targetValue", HTTP_GET, [](AsyncWebServerRequest *request) {
		request->send_P(200, "text/plain", String(targetValue).c_str());
	});
	server.on("/get", HTTP_GET, [](AsyncWebServerRequest *request) {
		String inputParam;
		// GET input1 value on <ESP_IP>/get?temptarget=<inputMessage>
		if (request->hasParam(PARAM_INPUT_1))
		{
			String buf = request->getParam(PARAM_INPUT_1)->value();
			targetValue = buf.toFloat();
			inputParam = PARAM_INPUT_1;
			Serial.print("targetTemp: ");
			Serial.println(targetValue);
		}
		else
		{
			targetValue = 0.0;
			inputParam = "none";
		}
		request->send(LittleFS, "/index.html", String(), false);
	});

	// Receive an HTTP GET request
	server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request) {
		Serial.println("on");
		regulState = 1;
		request->send(200, "text/plain", "ok");
	});

	// Receive an HTTP GET request
	server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request) {
		Serial.println("off");
		regulState = 0;
		request->send(200, "text/plain", "ok");
	});

	// Start server
	server.begin();
}

void loop()
{
	unsigned long currentMillis = millis();
	if (currentMillis - previousMillis >= interval)
	{
		// save the last time you updated the DHT values
		previousMillis = currentMillis;
		// Read temperature as Fahrenheit (isFahrenheit = true)
		//float newT = dht.readTemperature(true);
		// if temperature read failed, don't change t value
		// Read Humidity
		temperature = dht.readTemperature();
		// if temperature read failed, set temperature to -100
		if (isnan(temperature))
		{
			Serial.println("Failed to read from DHT sensor!");
			temperature = -100;
		}
		else
		{
			Serial.println(temperature);
		}

		// Read Humidity
		humidity = dht.readHumidity();
		// if humidity read failed, don't change h value
		if (isnan(humidity))
		{
			Serial.println("Failed to read from DHT sensor!");
			humidity = -100;
		}
		else
		{
			Serial.println(humidity);
		}

		// Update drying fan duty cycle
		dryingDC = pid(temperature, targetValue, COEFF_P, COEFF_I, COEFF_D);

		// check dryingDC min and max
		if(dryingDC < 100)
			dryingDC = 100;
		if(dryingDC > 1000)
			dryingDC = 1000;
		// Print drying duty cycle
		Serial.println(dryingDC / 10);

		// update pwm only when the dryer is on
		if (regulState)
		{

			digitalWrite(powerMosfet, LOW);

			// Update PWMs
			analogWrite(statusLed, dryingDC);
			analogWrite(dryingFan, dryingDC);
		}
		else
		{
			analogWrite(statusLed, 0);
			analogWrite(dryingFan, 0);

			digitalWrite(powerMosfet, HIGH);
		}
	}
}
